# golang-echo-rethinkdb

## build
```bash
go build src/main.go
```

## start rethink db
```bash
docker run -d -it --name echo-rethinkdb \
    -v "$PWD:/data" \
    -p 8081:8080 \
    -p 28015:28015 \
    -p 29015:29015 \
    rethinkdb:2.3.6
    
open http://localhost:8081/
```
