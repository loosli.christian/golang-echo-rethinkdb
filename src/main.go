package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	r "gopkg.in/rethinkdb/rethinkdb-go.v5"
)

type logWriter struct {
}

func (writer logWriter) Write(bytes []byte) (int, error) {
	return fmt.Print(time.Now().UTC().Format("2006-01-02T15:04:05.999Z") + " [DEBUG] " + string(bytes))
}

type Server struct {
	db *r.Session
}

func main() {
	log.SetFlags(0)
	log.SetOutput(new(logWriter))

	var config = map[string]string{}
	initConfigMap(config)

	db, err := connectToDb(config)
	if err != nil {
		log.Fatal(err)
	}

	s := &Server{db: db}

	listDatabases(s)

	e := echo.New()
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())
	e.Use(middleware.Gzip())
	e.Use(middleware.Logger())
	e.Use(middleware.RequestID())

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, Gopher!")
	})

	// Start server
	go func() {
		if err := e.Start(":1323"); err != nil {
			e.Logger.Info("shutting down the server")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 10 seconds.
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}
}

func initConfigMap(config map[string]string) {
	config["Host"] = os.Getenv("RETHINKDB_HOST")
	config["Port"] = os.Getenv("RETHINKDB_PORT")
	config["Database"] = os.Getenv("RETHINKDB_DATABASE")
	config["Username"] = os.Getenv("RETHINKDB_USERNAME")
	config["Password"] = os.Getenv("RETHINKDB_PASSWORD")
	if config["Host"] == "" {
		config["Host"] = "127.0.0.1"
	}
	if config["Port"] == "" {
		config["Port"] = "28015"
	}
	if config["Database"] == "" {
		config["Database"] = "echo_rethinkdb"
	}
	if config["Username"] == "" {
		config["Username"] = ""
	}
	if config["Password"] == "" {
		config["Password"] = ""
	}
}

func listDatabases(server *Server) {
	res, err := r.DBList().Run(server.db)
	if err != nil {
		log.Fatalf("dblist query error %v", err)
	}
	defer res.Close()
	var rows []interface{}
	err = res.All(&rows)
	if err != nil {
		log.Fatalf("could not fetch the databases %v", err)
	}
	log.Printf("db list: %q \n", rows)
}

func connectToDb(config map[string]string) (*r.Session, error) {
	hostPort := net.JoinHostPort(config["Host"], config["Port"])
	log.Println("Connecting to database at", hostPort)

	db, err := r.Connect(r.ConnectOpts{
		Address:    hostPort,
		Database:   config["Database"],
		Username:   config["Username"],
		Password:   config["Password"],
		InitialCap: 10,
		MaxOpen:    10,
	})
	if err != nil {
		log.Printf("could not connect to database %s, %v\n", hostPort, err)
	}

	var dbError error
	maxAttempts := 20
	for attempts := 1; attempts <= maxAttempts; attempts++ {
		dbError = db.Reconnect()
		if dbError == nil {
			break
		}
		log.Println(dbError)
		time.Sleep(time.Duration(attempts) * time.Second)
	}

	return db, dbError
}
